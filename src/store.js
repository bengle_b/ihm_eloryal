import Vue from 'vue'
import Vuex from 'vuex'

import * as actionsServer from './Vuex/actionsServer'
import * as mutationsServer from './Vuex/mutationsServer'
import * as gettersServer from './Vuex/gettersServer'

import * as actionsNotification from './Vuex/actionsNotification'
import * as mutationsNotification from './Vuex/mutationsNotification'
import * as gettersNotification from './Vuex/gettersNotification'

import * as actionsLog from './Vuex/actionsLog'
import * as mutationsLog from './Vuex/mutationsLog'
import * as gettersLog from './Vuex/gettersLog'

Vue.use(Vuex)
 
const moduleServer = {
	state: {
		servers: []
	},
	actions: actionsServer,
	mutations: mutationsServer,
	getters: gettersServer

}

const moduleNotification = {
	state: {
		notifications: []
	},
	actions: actionsNotification,
	mutations: mutationsNotification,
	getters: gettersNotification

}

const moduleLog = {
	state: {
		login: "",
		logged: false,
		token: ""
	},
	actions: actionsLog,
	mutations: mutationsLog,
	getters: gettersLog

}

let store = new Vuex.Store ({
	modules: {
		servers: moduleServer,
		notifications: moduleNotification,
		log: moduleLog
	},
	/*state: state,
	mutations: mutations,
	getters: getters,
	actions: actions,*/
  	strict: process.env.NODE_ENV !== 'production',
})

export default store