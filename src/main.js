import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Toasted from 'vue-toasted';
import store from './store'
import redirect from './redirect'
import * as configInterceptorHttp from './configHttp/configInterceptorHttp'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(Toasted)

configInterceptorHttp.configInterceptorHttp(); // HTTP INTERCEPTOR TRIGER NOTIFICATIONS WHEN REQUEST ARE SENT

Vue.filter('capitalize', function (value){      // EXAMPLE FILTER UPPERCASE
  console.log(value);
  return value;
});

const isLogged = function () {
  if (store.getters.isLogged) {
    return true //next();
  } else {
    return false; //next( { path : '/login' } );
  }
}

const router = new VueRouter ({
  mode: 'hash',
  routes: [
  { path: '/', component: require('./components/Home.vue'), name: 'home', meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: false  } }, ////////////////////// REQUIRE AUTH

  { path: '/login', component: require('./components/Login.vue'), name: 'log in', meta: { requiresAuth: false, checkValidId: false, checkServiceAndId: false  }},

  //{ path: '/signup', component: require('./components/Signup.vue'), name: 'sign up'},

  { path: '/network/:id(\\d+)', component: require('./components/Network/Network_Group.vue'), children: [
        { path: '', component: require('./components/Network/Network.vue'), name: 'network'},
        { path: 'add', component: require('./components/Network/Network_Add.vue'), name: 'network.add'},
        { path: 'edit', component: require('./components/Network/Network_Edit.vue'), name: 'network.edit'}

  ], meta: { requiresAuth: true, checkValidId: true, checkServiceAndId: false } }, ///////// REQUIRE AUTH


  { path: '/addserver', component: require('./components/AddServer.vue'), name: 'add server', meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: false  }}, ///////// REQUIRE AUTH

  { path: '/createserver', component: require('./components/CreateServer.vue'), name: 'create server', meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: false  }}, ///////// REQUIRE AUTH

  { path: '/settings/:id(\\d+)', component: require('./components/Settings.vue'), name: 'settings', meta: { requiresAuth: true, checkValidId: true, checkServiceAndId: false  } }, ///////// REQUIRE AUTH


  { path: '/dns/:id(\\d+)', component: require('./components/DNS/Dns_Group.vue'),
    children: [
      { path: '', component: require('./components/DNS/Dns.vue'), name: 'dns'},
      { path: 'addzone', component: require('./components/DNS/Dns_addzone.vue'), name: 'dns.addzone'},
      { path: 'editzone', component: require('./components/DNS/Dns_editzone.vue'), name: 'dns.editzone'},
      { path: 'entries', component: require('./components/DNS/DNS_EntryView.vue'), name: 'dns.entries'},
      { path: 'addentries', component: require('./components/DNS/DNS_EntryAdd.vue'), name: 'dns.addentries'},
      { path: 'editentries', component: require('./components/DNS/DNS_EntryEdit.vue'), name: 'dns.editentries'}
    ],
    meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "DNS" }
  }, ///////// REQUIRE AUTH


  { path: '/dhcp/:id(\\d+)', component: require('./components/Dhcp/Dhcp_Group.vue'),
  children: [
  { path: 'addzone', component: require('./components/Dhcp/Dhcp_AddZone.vue'), name: 'dhcp.addzone'},
  { path: 'editzone/:subnet(\\d+)', component: require('./components/Dhcp/Dhcp_EditZone.vue'), name: 'dhcp.editzone' },
  { path: '', component: require('./components/Dhcp/Dhcp.vue'), name: 'dhcp'},
  { path: 'editzone/:subnet(\\d+)/addhost', component: require('./components/Dhcp/Dhcp_AddHost.vue'), name: 'dhcp.addhost'},
  { path: 'editzone/:subnet(\\d+)/edithost', component: require('./components/Dhcp/Dhcp_EditHost.vue'), name: 'dhcp.edithost'}
  ],
  meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "DHCP" }  }, ///////// REQUIRE AUTH


  { path: '/sftp/:id(\\d+)', component: require('./components/SFTP/SFTP_Group.vue'),
  children: [
  { path: 'adduser', component: require('./components/SFTP/SFTP_adduser.vue'), name: 'sftp.adduser'},
  { path: 'edituser/:user(\\d+)', component: require('./components/SFTP/SFTP_edituser.vue'), name: 'sftp.edituser'},
  { path: 'adddirectory', component: require('./components/SFTP/SFTP_adddirectory.vue'), name: 'sftp.adddirectory'},
  { path: 'editdirectory', component: require('./components/SFTP/SFTP_editdirectory.vue'), name: 'sftp.editdirectory'},
  { path: '', component: require('./components/SFTP/SFTP.vue'), name: 'sftp'}
  ], meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "SFTP" }  }, ///////// REQUIRE AUTH


  { path: '/supervision/:id(\\d+)', component: require('./components/Supervision/Supervision_Group.vue'),
  children: [
  { path: 'addserver', component: require('./components/Supervision/Supervision_addserver.vue'), name: 'supervision.addserver'},
  { path: 'config', component: require('./components/Supervision/Supervision_config.vue'), name: 'supervision.config'},
  { path: 'agent', component: require('./components/Supervision/Agents.vue'), name: 'supervision.agents'},
  { path: '', component: require('./components/Supervision/Supervision.vue'), name: 'supervision'}
  ], meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "SUPERVISION"  }  }, ///////// REQUIRE AUTH


  { path: '/MAJ/:id(\\d+)', component: require('./components/MAJ/MAJ_Group.vue'),
  children: [
  { path: '', component: require('./components/MAJ/MAJ.vue'), name: 'maj'},
  { path: 'addrepo', component: require('./components/MAJ/MAJ_addrepo.vue'), name: 'maj.addrepo'}
  ], meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "MAJ" }
  }, ///////// REQUIRE AUTH


  { path: '/Web/:id(\\d+)', component: require('./components/Web/Web_group.vue'),
  children: [
  { path: '', component: require('./components/Web/Web.vue'), name: 'web'},
  { path: 'addvirtualhost', component: require('./components/Web/Web_addvirtualhost.vue'), name: 'web.addvirtualhost'},
  { path: 'editvirtualhost', component: require('./components/Web/Web_editvirtualhost.vue'), name: 'web.editvirtualhost'}
  ], meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "WEB" }
  }, ///////// REQUIRE AUTH


  { path: '/Proxy/:id(\\d+)', component: require('./components/Proxy\ Web/Proxy_group.vue'),
  children: [
  { path: '', component: require('./components/Proxy\ Web/Proxy.vue'), name: 'proxy'},
  { path: 'addpool', component: require('./components/Proxy\ Web/Proxy_addpool.vue'), name: 'proxy.addpool'},
  { path: 'editpool', component: require('./components/Proxy\ Web/Proxy_editpool.vue'), name: 'proxy.editpool'},
  { path: 'addmember', component: require('./components/Proxy\ Web/Proxy_addmember.vue'), name: 'proxy.addmember'},
  { path: 'editmember', component: require('./components/Proxy\ Web/Proxy_editmember.vue'), name: 'proxy.editmember'}
  ], meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "PROXY" }
  }, ///////// REQUIRE AUTH

  { path: '/LDAP/:id(\\d+)', component: require('./components/LDAP/LDAP_group.vue'),
  children: [
    { path: '', component: require('./components/LDAP/LDAP.vue'), name: 'ldap'},
    { path: 'addgroup', component: require('./components/LDAP/LDAP_addgroup.vue'), name: 'ldap.addgroup'},
    { path: 'editgroup', component: require('./components/LDAP/LDAP_editgroup.vue'), name: 'ldap.editgroup'},
    { path: 'adduser', component: require('./components/LDAP/LDAP_adduser.vue'), name: 'ldap.adduser'},
    { path: 'edituser', component: require('./components/LDAP/LDAP_edituser.vue'), name: 'ldap.edituser'},
  ], meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "LDAP" }
  }, ///////// REQUIRE AUTH

  { path: '/SAMBA/:id(\\d+)', component: require('./components/Samba/Samba_group.vue'),
    children: [
      { path: '', component: require('./components/Samba/Samba.vue'), name: 'samba'},
      { path: 'addshare', component: require('./components/Samba/Samba_addshare.vue'), name: 'samba.addshare'},
      { path: 'editshare', component: require('./components/Samba/Samba_editshare.vue'), name: 'samba.editshare'},
    ], meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "SAMBA"}
  }, //////// REGUIRE AUTH

  { path: '/Inventory/:id(\\d+)', component: require('./components/Inventory/Inventory.vue'), name: 'inventory', meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "INVENTORY" }}, ///////// REQUIRE AUTH
  { path: '/Ticketing/:id(\\d+)', component: require('./components/Ticketing/Ticketing.vue'), name: 'ticketing', meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: true, service: "TICKET" }}, ///////// REQUIRE AUTH

  { path: '/postman', component: require('./components/postman.vue'), name: 'postman', meta: { requiresAuth: true, checkValidId: false, checkServiceAndId: false }}, ///////// REQUIRE AUTH

  //{ path: '/logout', component: require('./components/Logout.vue'), name: 'log out'},

  { path: '*', redirect: '/' }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && isLogged()) {

    if (to.matched.some(record => record.meta.checkServiceAndId) )
    {

      if (redirect.checkServiceAndId(to, to.matched[0].meta.service))
        next();
      else {
        console.log("redirect.checkServiceAndId to home");
        next({name: 'home'});
      }
    }
    else if (to.matched.some(record => record.meta.checkValidId))
    {
     if (redirect.checkValidIdUrl(to))
      next();
    else {
      console.log("redirect.checkValidIdUrl to home");
      next({name: 'home'});
    }
  }
  else
    next();
}
else if (to.matched.some(record => record.meta.requiresAuth) && !isLogged()) {
  next('login');
} else {
  next();
}
})


Vue.http.options.root = process.env.NODE_ENV === "production" ? "/web/app.php" : "http://ihmtestcent.eloryal.fr/web/app.php"
//Vue.http.options.root = "http://ihmtestcent.eloryal.fr/web/app.php"
/* IF PROD BUILD THEN HTTP.GET MUST POINTED TO THE CURRENT SERVER TO PROCESS HTTP REQUEST */

Vue.http.headers['token'] = "";
Vue.http.headers.common.token = ""

//Vue.http.headers.post['token'] = "";

/* eslint-disable no-new */
let vue = new Vue ({
  store,
  el: '#app',
  template: '<App/>',
  router,
  render: h => h(require('./App'))
})

export default vue
