import Vue from 'vue'
import VueResource from 'vue-resource'
import * as Url from './NotificationUrl'
import store from '../store'

export const configInterceptorHttp = () => {
	var url = "";
	var my_request_body = {};

	Vue.http.interceptors.push(function(request, next) {

		url = request.url;		// GET THE URL EXTENSION IN ORDER TO FILTER THE ONES WE WANT TO TRIGER NOTIFICATIONS
		my_request_body = request.body;

		next(function(response) {

			Url.NotificationUrl.forEach((element, index) => {

				if (url == element.url && response.status >= 200 && response.status <= 299 && element.hasOwnProperty("info")) { 									// INFORMATION NOTIFICATION
					infoNotification(index, my_request_body, response);
				} else if (url == element.url && response.status >= 200 && response.status <= 299 && element.hasOwnProperty("success")) {							// SUCCESS NOTIFICATION
					successNotification(index, my_request_body);
				} else if (url == element.url && response.status >= 400 && response.status <= 600) {																// ERROR NOTIFICATION
					errorNotification(index, my_request_body, response);
				}
			});

		});
	});
}

export const successNotification = (index, my_request_body) => {
	var content = Url.NotificationUrl[index].isPathIntoRequest ? my_request_body[Url.NotificationUrl[index].pathIntoRequest].toString() + Url.NotificationUrl[index].success :
	Url.NotificationUrl[index].success;

	let toast = Vue.toasted.success(content, {
		theme: "bubble",
		position: "top-right",
		duration : 8000,
		icon : { name : 'check_circle'},
		action : [{icon : 'clear',
		onClick : (e) => {
			toast.goAway(0);
		}
	}]
});
	
	store.dispatch('addNotif', {"method": "success", "title": "", "content": content, "checked": false});
}

export const errorNotification = (index, my_request_body, response) => {
	let toast = Vue.toasted.error("<div class=\"ui grid\"><div class=\"row\"><div class=\"column\" style=\"margin-top: 3px; text-decoration: underline;\"><h3>Something went wrong</h3></div></div><div class=\"center aligned row\"><div class=\"fourteen wide column centered\" style=\"margin-bottom: 3px; font-style: italic;\">" + response.body.error_msg + "</div></div></div>", {
		theme: "bubble",
		position: "top-right", 
		duration : null,
		icon : { name : 'warning'},
		action : [
		{icon : 'clear',
		onClick : (e) => {
			toast.goAway(0);
		}
	}],
	containerClass: 'customeContainer'
});
	store.dispatch('addNotif', {"method": "error", "title": "Operation failed", "content": response.body.error_msg, "checked": false});
}

export const infoNotification = (index, my_request_body, response) => {
	var title = Url.NotificationUrl[index].isPathIntoRequest ? my_request_body[Url.NotificationUrl[index].pathIntoRequest] + Url.NotificationUrl[index].info :
	Url.NotificationUrl[index].info;

console.log(response.body.response);
	var content = JSON.stringify(response.body.response, null, "<br>");


	content = content.replace(/,/g, "").replace(/{/g, "").replace(/}/g).replace(/\"/g, "").replace(/\\n/g, "<br>").replace(/undefined/g, "") + "<br><br>";
	console.log(content);

	let toast = Vue.toasted.info("<div class=\"ui grid\" style=\"max-height: 800px; overflow-y: auto\"><div class=\"row\"><div class=\"column\" style=\"margin-top: 3px; text-decoration: underline;\"><h3>" + title + "</h3></div></div><div class=\"center aligned row\"><div class=\"fourteen wide column centered\" style=\"margin-bottom: 3px; font-style: italic;\">" + content + "</div></div></div>", {
		theme: "bubble",
		position: "top-right", 
		duration : null,
		icon : { name : 'info'},
		action : [
		{icon : 'clear',
		onClick : (e) => {
			toast.goAway(0);
		}
	}],
	containerClass: 'customeContainer',
	className: 'customeClassInfo'
});
	store.dispatch('addNotif', {"method": "info", "title": title, "content": content.replace(/<br>/g, ""), "checked": false});
}
