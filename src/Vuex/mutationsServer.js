	import Vue from 'vue'

/*SET_LOGGEDIN: (state, logged) => {
		state.loggedIn = logged;
		localStorage.token = logged;
	},*/

	export const SET_SERVERS = (state, servers) => {
		state.servers = servers;
	}

	export const SET_SERVER = (state, payload) => {
		// payload.hostname | payload.service | payload.ip | payload.idx
		state.servers[payload.idx].hostname = payload.hostname;
		state.servers[payload.idx].service = payload.service.toUpperCase();
		state.servers[payload.idx].ip = payload.ip;
	}

	export const ADD_SERVER = (state, server) => {
		state.servers.push(server);
	}

	export const CLEAN = (state) => {
		state.servers = [];
	}

	export const DELETE_SERVER = (state, my_server) => {
		let idx;

		if ((idx = state.servers.findIndex((server) => {
			return server == my_server;
		})) != -1) {
			state.servers.splice(idx, 1);
		}
	}

	export const ADD_CONF_DNS = (state, payload) => {
		// payload.server | payload.conf_dns
		if (!payload.server.hasOwnProperty("conf_dns"))
			Vue.set(payload.server, "conf_dns", {});
		payload.server.conf_dns = payload.conf_dns;
	}

	export const ADD_ZONE_ENTRIES_DNS = (state, payload) => {
		//payload.server | payload.zone | payload.entry
		if (!payload.server.conf_dns.zone[payload.zone.name].hasOwnProperty("zone_entry"))
			Vue.set(payload.server.conf_dns.zone[payload.zone.name], "zone_entry", {});
		payload.server.conf_dns.zone[payload.zone.name].zone_entry = payload.entry;
	}

	export const ADD_ZONE_ENTRY = (state, payload) => {
		//payload.server | payload.zone | payload.entry
		if (!payload.server.conf_dns.zone[payload.zone.name].hasOwnProperty("zone_entry"))
			Vue.set(payload.server.conf_dns.zone[payload.zone.name], "zone_entry", {});
		if (!payload.server.conf_dns.zone[payload.zone.name].zone_entry.hasOwnProperty(payload.entry.value))
			Vue.set(payload.server.conf_dns.zone[payload.zone.name].zone_entry, payload.entry.value, payload.entry);
		else
			payload.server.conf_dns.zone[payload.zone.name].zone_entry[payload.entry.value] = payload.entry;
	}

	export const EDIT_ZONE_ENTRY = (state, payload) => {
		//payload.server | payload.zone | payload.new_entry | payload.old_entry_index
		if (!payload.server.conf_dns.zone[payload.zone.name].hasOwnProperty("zone_entry"))
			Vue.set(payload.server.conf_dns.zone[payload.zone.name], "zone_entry", {});
		if (payload.server.conf_dns.zone[payload.zone.name].zone_entry.hasOwnProperty(payload.old_idx_entry))
			payload.server.conf_dns.zone[payload.zone.name].zone_entry.splice(payload.old_idx_entry, 1);
		if (!payload.server.conf_dns.zone[payload.zone.name].zone_entry.hasOwnProperty(payload.new_entry.value))
			Vue.set(payload.server.conf_dns.zone[payload.zone.name].zone_entry, payload.new_entry.value, payload.new_entry);
		else
			payload.server.conf_dns.zone[payload.zone.name].zone_entry[payload.new_entry.value] = payload.new_entry;
	}

	export const DELETE_ZONE_ENTRY = (state, payload) => {
		//payload.server | payload.zone | payload.idx_entry
		if (payload.server.conf_dns.zone[payload.zone.name].zone_entry.hasOwnProperty(payload.idx_entry))
			payload.server.conf_dns.zone[payload.zone.name].zone_entry.splice(payload.idx_entry, 1);
	}

	export const ADD_ZONE_SOA_ENTRY_DNS = (state, payload) => {
		//payload.server | payload.zone | payload.entry
		if (!payload.server.conf_dns.zone[payload.zone.name].hasOwnProperty("SOA"))
			Vue.set(payload.server.conf_dns.zone[payload.zone.name], "SOA", {});
		payload.server.conf_dns.zone[payload.zone.name].SOA = payload.entry;
	}

	export const EDIT_ZONE_SOA = (state, payload) => {
		//payload.server | payload.zone | payload.conf
		payload.server.conf_dns.zone[payload.zone.name].SOA.TTL = payload.conf.TTL;
		Object.keys(payload.conf.soa).forEach(function(element, index) {
			if (!payload.server.conf_dns.zone[payload.zone.name].SOA.SOA.hasOwnProperty(element))
				Vue.set(payload.server.conf_dns.zone[payload.zone.name].SOA.SOA, element, typeof payload.conf.soa[element]);
			payload.server.conf_dns.zone[payload.zone.name].SOA.SOA[element] = payload.conf.soa[element];
		});
	}

	export const EDIT_CONF_DNS = (state, payload) => {
		// payload.server | payload.option_conf
		console.log(payload);
		if (!payload.server.conf_dns.hasOwnProperty("options"))
			Vue.set(payload.server.conf_dns, "options", {});

		Object.keys(payload.options).forEach( function(element, index) {
			if (!payload.server.conf_dns.options.hasOwnProperty(element))
				Vue.set(payload.server.conf_dns.options, element, typeof payload.options[element]);
			payload.server.conf_dns.options[element] = payload.options[element];
		});
	}

	export const ADD_EDIT_DNS_ZONE = (state, payload) => {
			// payload.server | payload.conf
		if (!payload.server.conf_dns.hasOwnProperty("zone"))
			Vue.set(payload.server.conf_dns, "zone", {});
		if (!payload.server.conf_dns.zone.hasOwnProperty(payload.conf.name))
			Vue.set(payload.server.conf_dns.zone, payload.conf.name, payload.conf);
		else
			payload.server.conf_dns.zone[payload.conf.name] = payload.conf;
	}

	export const DELETE_DNS_ZONE = (state, payload) => {
		console.log(payload, payload.zone_name);
		if (payload.server.conf_dns.zone.hasOwnProperty(payload.zone_name))
			delete payload.server.conf_dns.zone[payload.zone_name];
	}

	export const GET_SUBNETS = (state, payload) => {
		// payload.server | payload.subnets
		payload.server.subnets = payload.subnets;
		//Vue.set(payload.server, "subnets", payload.subnets);
	}

	export const ADD_SUBNET = (state, payload) => {
		// payload.server | payload.subnet
		if (!payload.server.hasOwnProperty("subnets"))
			Vue.set(payload.server, "subnets", []);
		payload.server.subnets.push(payload.subnet);
	}

	export const EDIT_SUBNET = (state, payload) => {
		// payload.server | payload.new_subnet | payload.idx_subnet
		payload.server.subnets[payload.idx_subnet] = payload.new_subnet;
	}

	export const DELETE_SUBNET = (state, payload) => {
		// payload.server | payload.idx_subnet
		payload.server.subnets.splice(payload.idx_subnet, 1);
	}

	export const ADD_SFTP_USER = (state, payload) => {
		//payload.server | payload.user
		if (!payload.server.hasOwnProperty("users"))
			Vue.set(payload.server, "users", []);
		payload.server.users.push(payload.user);
	}

	export const DELETE_SFTP_USER = (state, payload) => {
		//payload.server | payload.id_user
		payload.server.users.splice(payload.id_user, 1);
	}

	export const DELETE_SFTP_DIRECTORY = (state, payload) => {
		//payload.server | payload.id_directory
		payload.server.shared_directories.splice(payload.id_directory, 1);

	}

	export const ADD_SFTP_SHARED_DIRECTORY = (state, payload) => {
		//payload.server | payload.shared_directory
		if (!payload.server.hasOwnProperty("shared_directories"))
			Vue.set(payload.server, "shared_directories", []);
		payload.server.shared_directories.push(payload.shared_directory);
		if (!payload.server.shared_directories[payload.server.shared_directories.length - 1].hasOwnProperty("associated_users"))
			Vue.set(payload.server.shared_directories[payload.server.shared_directories.length - 1], "associated_users", []);
	}

	export const ADD_SFTP_USER_IN_DIRECTORY = (state, payload) => {
 		// payload.directory | payload.user
 		if (!payload.directory.hasOwnProperty("associated_users"))
 			Vue.set(payload.directory, "associated_users", []);
 		payload.directory.associated_users.push(payload.user);
 	}

 	export const DELETE_SFTP_USER_IN_DIRECTORY = (state, payload) => {
		// payload.directory | payload.idx_user
		console.log(payload);
		if (payload.directory.hasOwnProperty("associated_users")) {
			payload.directory.associated_users.splice(payload.idx_user, 1);
		}
	}

	export const ADD_INVENTORY_AGENTS = (state, payload) => {
		// payload.server | payload.inventory_agents
		if (!payload.server.hasOwnProperty("inventory_agents"))
			Vue.set(payload.server, "inventory_agents", {});
		payload.server.inventory_agents = payload.inventory_agents;
	}

	export const DELETE_INVENTORY_AGENT = (state, payload) => {
		// payload.server | payload.idx_agent
		if (payload.server.hasOwnProperty("inventory_agents")) {
			payload.server.inventory_agents.splice(payload.idx_agent, 1);
		}
	}

	export const ADD_SUPERVISION_AGENTS = (state, payload) => {
		// payload.server | payload.supervision_agents
		if (!payload.server.hasOwnProperty("supervision_agents"))
			Vue.set(payload.server, "supervision_agents", {});
		payload.server.supervision_agents = payload.supervision_agents;
	}

	export const DELETE_SUPERVISION_AGENT = (state, payload) => {
		// payload.server | payload.idx_agent
		console.log(payload);
		if (payload.server.hasOwnProperty("supervision_agents")) {
			payload.server.supervision_agents.splice(payload.idx_agent, 1);
		}
	}

	export const ADD_SUPERVISION_CONF = (state, payload) => {
		// payload.server | payload.supervision_config
		if (!payload.server.hasOwnProperty("supervision_config"))
			Vue.set(payload.server, "supervision_config", {});
		payload.server.supervision_config = payload.supervision_config;
	}

	export const EDIT_SUPERVISION_CONF = (state, payload) => {
		// payload.server | payload.new_conf
		payload.server.supervision_config = payload.new_conf;
	}

	export const ADD_TICKETING_CONF = (state, payload) => {
		// payload.server | payload.ticketing_config
		if (!payload.server.hasOwnProperty("ticketing_config"))
			Vue.set(payload.server, "ticketing_config", {});
		payload.server.ticketing_config = payload.ticketing_config;
	}

	export const EDIT_TICKETING_CONF = (state, payload) => {
		// payload.server | payload.new_conf
		payload.server.ticketing_config["MantisBT Email Settings"] = payload.new_conf["MantisBT Email Settings"];
		payload.server.ticketing_config["MantisBT Database Settings"] = payload.new_conf["MantisBT Database Settings"];
	}

	export const ADD_LDAP_USER = (state, payload) => {
		//payload.server | payload.user
		if (!payload.server.hasOwnProperty("ldap_users"))
			Vue.set(payload.server, "ldap_users", []);
		payload.server.ldap_users.push(payload.user);
		if (!payload.server.ldap_users[payload.server.ldap_users.length - 1].hasOwnProperty("groups"))
			Vue.set(payload.server.ldap_users[payload.server.ldap_users.length - 1], "groups", []);
	}

	export const ADD_LDAP_ORGANIZATION_UNIT = (state, payload) => {
		//payload.server | payload.group
		if (!payload.server.hasOwnProperty("ldap_groups"))
			Vue.set(payload.server, "ldap_groups", []);
			console.log(payload.group);
		payload.server.ldap_groups.push(payload.group);
		if (!payload.server.ldap_groups[payload.server.ldap_groups.length - 1].hasOwnProperty("members"))
			Vue.set(payload.server.ldap_groups[payload.server.ldap_groups.length - 1], "members", []);
	}

	export const DELETE_LDAP_USER = (state, payload) => {
		//payload.server | payload.id_user
		payload.server.ldap_users.splice(payload.id_user, 1);
	}

	export const DELETE_LDAP_GROUP = (state, payload) => {
		//payload.server | payload.id_group
		payload.server.ldap_groups.splice(payload.id_group, 1);

	}

	export const ADD_LDAP_USER_IN_GROUP = (state, payload) => {
 		// payload.group | payload.user
 		if (!payload.group.hasOwnProperty("members"))
 			Vue.set(payload.group, "members", []);
 		payload.group.members.push(payload.user);
 	}

 	export const DELETE_LDAP_USER_IN_GROUP = (state, payload) => {
		// payload.group | payload.idx_user
		console.log(payload);
		if (payload.group.hasOwnProperty("members")) {
			payload.group.members.splice(payload.idx_user, 1);
		}
	}

	export const SAMBA_CONFIGURATION = (state, payload) => {
		//payload.server | payload.samba_conf
		console.log(payload);
		payload.server.samba_conf = payload.samba_conf;
	}

	export const SAMBA_ADD_SHARE = (state, payload) => {
		//payload.server | payload.share
		console.log(payload.server);
		if (!payload.server.hasOwnProperty("samba_conf"))
			Vue.set(payload.server, "samba_conf", {});
		payload.server.samba_conf[payload.share.name] = payload.share;
	}

	export const SAMBA_UPDATE_SHARE = (state, payload) => {
		//payload.server | payload.share
		console.log(payload);
		if (payload.server.hasOwnProperty("samba_conf")) {
			if (payload.server.samba_conf.hasOwnProperty(payload.old_share_name))
				payload.server.samba_conf[payload.old_share_name] = payload.new_share;
		}
	}

	export const SAMBA_DELETE_SHARE = (state, payload) => {
		//payload.server | payload.share
		console.log(payload);
		if (payload.server.hasOwnProperty("samba_conf")) {
			if (payload.server.samba_conf.hasOwnProperty(payload.share_name)) {
				Vue.delete(payload.server.samba_conf, payload.share_name);
			}
		}
	}


