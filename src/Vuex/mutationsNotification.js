import Vue from 'vue'

export const ADD_NOTIF = (state, notif) => {
		if (!notif.hasOwnProperty("id"))
			Vue.set(notif, "id", 0);
		notif.id = state.notifications.length;
		state.notifications.push(notif);
	}