import Vue from 'vue'

export const SET_LOGIN = (state, logconfig) => {
	state.login = logconfig.login;
	state.token = logconfig.token;
	state.logged = true;
	Vue.http.headers['token'] = state.token;
	Vue.http.headers.common.token = state.token;
	//Vue.http.headers.post['token'] = state.token;
}

export const LOGOUT = (state) => {
	state.login = "";
	state.token = "";
	state.logged = false;
	Vue.http.headers['token'] = '';
	Vue.http.headers.common.token = '';
	//Vue.http.headers.post['token'] = "";
}