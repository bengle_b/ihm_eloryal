import Vue from 'vue'

export const addNotif = ( {commit, state}, notif ) => {
	return new Promise((resolve, reject) => {
		commit('ADD_NOTIF', {"method": notif.method, "title": notif.title, "content": notif.content, "checked": false} );
	})
}