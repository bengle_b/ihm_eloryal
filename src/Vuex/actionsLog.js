import Vue from 'vue'

export const setLogin = ( {commit, state}, logconfig ) => {
	return new Promise((resolve, reject) => {
		commit('SET_LOGIN', {"login": logconfig.login, "token": logconfig.token } );
	})
}

export const logout = ( {commit, state} ) => {
	return new Promise((resolve, reject) => {
		commit('LOGOUT');
	})
}