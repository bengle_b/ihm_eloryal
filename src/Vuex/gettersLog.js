export const isLogged = state => state.logged

export const getLogin = state => state.login

export const getToken = state => state.token