import Vue from 'vue'

export const loadServers = ( {commit, state} ) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('servers', {}).then((response) => {
			resolve(response);
			for (var i = 0; i < response.body.response.length; i++) {
				commit('ADD_SERVER', {"hostname": response.body.response[i].name, "service": response.body.response[i].service.toUpperCase(), "ip": response.body.response[i].ip});
			};
		},
		(response) => {
			console.log("error retrieveServers function");
			reject(response);
		});
	})
}

export const set_LoggedIn = ( {commit, state}, logged ) => {
	store.commit('SET_LOGGEDIN', logged)
}

export const set_Servers = ({commit, state}, servers) => {
	commit('SET_SERVERS', servers);
}

export const set_Server = ( {commit, state }, server) => {
	commit('SET_SERVER', server);
}

export const add_Server = function ( {commit}, server) {
	commit('ADD_SERVER', server);
}

export const clean = ( {commit, state} ) => {
	commit('CLEAN');
}

export const get_ConfDns = ( {commit, state }, server) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dns', {"ip": server.ip } ).then((response) => {
			resolve(response);
			commit('ADD_CONF_DNS', {"server": server, "conf_dns": response.body.response });
		},
		(error) => {
			reject(error);
		});
	});
}

export const get_ZoneEntries = ( {commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dns/entry/get', {"ip": payload.server.ip, "zone_name": payload.zone.name }).then((response) => {
			resolve(response);
			commit('ADD_ZONE_ENTRIES_DNS', {'server': payload.server, "zone": payload.zone, "entry": response.body.response})
		},
		(error) => {
			reject(error);
		});
	});
}

export const get_ZoneEntrySOA = ( {commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dns/entry_soa/get', {"ip": payload.server.ip, "zone_name": payload.zone.name }).then((response) => {
			resolve(response);
			commit('ADD_ZONE_SOA_ENTRY_DNS', {'server': payload.server, "zone": payload.zone, "entry": response.body.response})
		},
		(error) => {
			reject(error);
		});
	});
}

export const add_zone_entry = ( {commit, state }, payload) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dns/entry/add', {"ip": payload.server.ip, "zone_name": payload.zone.name, "entry": payload.entry}).then((response) => {
			resolve(response);
			commit('ADD_ZONE_ENTRY', {"server": payload.server, "zone": payload.zone, "entry": payload.entry});
		},
		(error) => {
			reject(error);
		});
	});
}

export const edit_zone_entry = ( {commit, state }, payload) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dns/entry/edit', {"ip": payload.server.ip, "zone_name": payload.zone.name, "new_entry": payload.new_entry, "old_entry": payload.old_entry}).then((response) => {
			resolve(response);
			commit('EDIT_ZONE_ENTRY', {"server": payload.server, "zone": payload.zone, "new_entry": payload.new_entry, "old_entry_index": payload.idx_old});
		},
		(error) => {
			reject(error);
		});
	});
}

export const edit_zone_soa = ( {commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dns/entry_soa/edit', payload.conf).then((response) => {
			resolve(response);
			commit('EDIT_ZONE_SOA', {"server": payload.server, "zone": payload.zone, "conf": payload.conf});
		},
		(error) => {
			reject(error);
		});
	});
}

export const delete_zone_entry = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		console.log({"ip": payload.server.ip, "zone_name": payload.zone.name, "entry": payload.server.conf_dns.zone[payload.zone.name].zone_entry[payload.idx_entry]});
		let entry = {
			"owner": payload.server.conf_dns.zone[payload.zone.name].zone_entry[payload.idx_entry].owner,
			"type": payload.server.conf_dns.zone[payload.zone.name].zone_entry[payload.idx_entry].type,
			"value": payload.server.conf_dns.zone[payload.zone.name].zone_entry[payload.idx_entry].name
		};
		Vue.http.post('dns/entry/delete', {"ip": payload.server.ip, "zone_name": payload.zone.name, "entry": entry}).then((response) => {
			resolve(response);
			commit('DELETE_ZONE_ENTRY', {"server": payload.server, "zone": payload.zone, "idx_entry": payload.idx_entry});
		},
		(error) => {
			reject(error);
		});
	});
}

export const edit_ConfDns = ( {commit, state }, payload) => {
	return new Promise((resolve, reject) => {
		console.log(payload);
		Vue.http.post('dns/edit', payload.conf).then((response) => {
			resolve(response);
			commit('EDIT_CONF_DNS', {"server": payload.server, "options": payload.conf.conf });
		},
		(error) => {
			reject(error);
		});
	});
}

export const add_edit_DnsZone = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dns/zone/edit', {"ip": payload.edit_zone.ip, "zone_name": payload.edit_zone.zone_name, "conf": payload.edit_zone.conf }).then((response) => {
			resolve(response);
			commit('ADD_EDIT_DNS_ZONE', {"server": payload.server, "conf": payload.edit_zone.conf} )
		},
		(error) => {
			reject(error);
		});
	})
}

export const delete_DnsZone = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dns/zone/delete', {"ip": payload.del_dns_zone.ip , "zone_name": payload.del_dns_zone.zone_name } ).then((response) => {
			resolve(response);
			commit('DELETE_DNS_ZONE', { "server": payload.server, "zone_name": payload.del_dns_zone.zone_name });
		},
		(error) => {
		});
	});
}

export const get_Subnets = ( {commit, state}, server ) => {
	return new Promise((resolve, reject) => {
		Vue.http.post('dhcp/subnets', {"ip": server.ip } ).then((response) => {
			resolve(response);
			response.body.response.forEach( function(element, index) {
				if (element.hasOwnProperty("subnet") && element.subnet.length > 0)
					commit('ADD_SUBNET', { "server": server, "subnet": response.body.response[index] });
			});
		},
		(response) => {
			reject(response);
		});
	});
}

export const add_Subnet = ( {commit, state}, payload ) => {
		// payload.server | payload.subnet
		return new Promise((resolve, reject) => {
			Vue.http.post('dhcp/subnets/add', {"ip": payload.server.ip, "subnet": payload.subnet} ).then((response) => {
				resolve(response);
				commit('ADD_SUBNET', payload);
			},
			(response) => {
				reject(response);
			});
		});
	}

	export const edit_Subnet =  ( {commit, state }, payload) => {
		// payload.server | payload.new_subnet | payload.idx_subnet
		//console.log(payload);
		console.log( {"ip": payload.server.ip, "subnet": payload.new_subnet, "subnet_origin_ip": payload.server.subnets[payload.idx_subnet].subnet} );
		return new Promise((resolve, reject) => {
			Vue.http.post('dhcp/subnets/edit', {"ip": payload.server.ip, "subnet": payload.new_subnet, "subnet_origin_ip": payload.server.subnets[payload.idx_subnet].subnet} ).then((response) => {
				resolve(response);
				commit('EDIT_SUBNET', payload);
			},
			(response) => {
				reject(response);
			});
		});
	}

	export const delete_Subnet = ( {commit, state }, payload) => {
		// payload.server | payload.idx_subnet
		return new Promise((resolve, reject) => {
			Vue.http.post('dhcp/subnets/delete', {"ip": payload.server.ip, "subnet_ip": payload.server.subnets[payload.idx_subnet].subnet } ).then((response) => {
				resolve(response);
				commit('DELETE_SUBNET', payload);
			},
			(response) => {
				reject(response);
			});
		});
	}

	export const delete_Server =  ( {commit, state }, server) => {
		return new Promise((resolve, reject) => {
			commit('DELETE_SERVER', server);
			resolve();
		})
	}

	export const get_SFTP_users = ( {commit, state}, server) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('sftp/users', {"ip": server.ip } ).then((response) => {
				resolve(response);
				response.body.response.forEach( function(element, index) {

					commit('ADD_SFTP_USER', { "server": server, "user": response.body.response[index] });
				});
			},
			(response) => {
				reject(response);
			});
		});
	}

	export const get_SFTP_sharedDirectories = ( {commit, state }, server) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('sftp/folders', {"ip": server.ip } ).then((response) => {
				response.body.response.shared_folders.forEach( function(element, index) {

					commit('ADD_SFTP_SHARED_DIRECTORY', { "server": server,
						"shared_directory": response.body.response.shared_folders[index] });
				});
				resolve(response);
			},
			(response) => {
				reject(response);
			});
		});
	}

	export const get_SFTP_usersInsharedDirectories = ( {commit, state }, payload) => {
		// payload.server | payload.directory | payload.idx_directory
		return new Promise((resolve, reject) => {
			Vue.http.post('sftp/shared_folders/users', { "ip": payload.server.ip, "name_folder": payload.directory } ).then((response) => {
				resolve(response);
				response.body.response.forEach( function(element, index) {
					commit("ADD_SFTP_USER_IN_DIRECTORY", { "directory": payload.server.shared_directories[payload.idx_directory], "user": element});
				});
			},
			(response) => {
				reject(response);
			});
		});
	}

	export const add_SFTP_user = ({commit, state }, payload) => {
		// payload.server | payload.login | payload.passwd
		return new Promise((resolve, reject) => {
			console.log({"ip": payload.server.ip, "login": payload.login, "passwd": payload.passwd});
			Vue.http.post('sftp/users/add', {"ip": payload.server.ip, "login": payload.login, "passwd": payload.passwd} ).then((response) => {
				commit('ADD_SFTP_USER', {"server":  payload.server, "user": {"login": payload.login, "right": {}} });
				resolve(response);

			},
			(response) => {
				reject(response);
			});
		});
	}

	export const delete_SFTP_users = ( {commit, state }, payload) => {
		return new Promise((resolve, reject) => {

			/*for (var i = 0; i < payload.server.shared_directories.length; i++) {
				if (payload.server.shared_directories[i].associated_users.indexOf(payload.login) != -1) {
					console.log(payload.server.shared_directories[i].associated_users.indexOf(payload.login));
					delete_SFTP_usersInDirectory({commit, state}, {"server": payload.server, "idx_directory": i,
						"idx_user": payload.server.shared_directories[i].associated_users.indexOf(payload.login), "login": payload.login }).then(response => {

						}, error => {
							reject(error);
						});
				}
			}*/
			Vue.http.post('sftp/users/delete', {"ip": payload.server.ip, "login": payload.login } ).then((response) => {

				commit('DELETE_SFTP_USER', {"server": payload.server, "id_user": payload.id_user});
				resolve(response);

			},
			(response) => {
				reject(response);
			});
		});
	}

	export const add_SFTP_usersInDirectory = ( {commit, state }, payload) => {
		// payload.server | payload.idx_directory | payload.login
		return new Promise((resolve, reject) => {
			Vue.http.post('sftp/shared_folders/users/add', {"ip": payload.server.ip,
				"name_folder": payload.server.shared_directories[payload.idx_directory].name, "login": payload.login} ).then((response) => {
					commit("ADD_SFTP_USER_IN_DIRECTORY", { "directory": payload.server.shared_directories[payload.idx_directory], "user": payload.login});
					resolve(response);
				},
				(response) => {
					reject(response);
				});
			});
	}

	export const delete_SFTP_usersInDirectory = ( {commit, state }, payload) => {
		// payload.server | payload.idx_directory | payload.idx_user | payload.login
		console.log(payload, {"ip": payload.server.ip,
			"name_folder": payload.server.shared_directories[payload.idx_directory].name, "login": payload.login});
		return new Promise((resolve, reject) => {
			Vue.http.post('sftp/shared_folders/users/delete', {"ip": payload.server.ip,
				"name_folder": payload.server.shared_directories[payload.idx_directory].name, "login": payload.login} ).then((response) => {
					resolve(response);
					commit("DELETE_SFTP_USER_IN_DIRECTORY", { "directory": payload.server.shared_directories[payload.idx_directory], "idx_user": payload.idx_user});			},
					(response) => {
						reject(response);
					});
			});
	}

	export const add_SFTP_sharedDirectory = ( {commit, state }, payload) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('sftp/shared_folders/add', { "ip": payload.server.ip, "name_folder": payload.directory} ).then((response) => {
				commit("ADD_SFTP_SHARED_DIRECTORY", { "server": payload.server, "shared_directory": { "name": payload.directory } } );
				resolve(response);
			},
			(response) => {
				reject(response);
			});
		});
	}

	export const get_agent_inventory = ( {commit, state}, server ) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('inventory/agents', {'ip': server.ip } ).then((response) => {
				resolve(response);
				commit('ADD_INVENTORY_AGENTS', {"server": server, "inventory_agents": response.body.response });
			},
			(error) => {
				reject(error);
			});
		});
	}

	export const delete_inventoryAgent = ( {commit, state}, payload) => {
		// payload.server | payload.idx_agent
		return new Promise((resolve, reject) => {
			Vue.http.post('inventory/agents/uninstall', {'ip': payload.server.ip, 'agent_ip': payload.server.inventory_agents[payload.idx_agent] }).then((response) => {
				resolve(response);
				commit('DELETE_INVENTORY_AGENT', {'server': payload.server });
			});
		});
	}

	export const get_agent_supervision = ( {commit, state}, server) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('supervision/agents', {'ip': server.ip}).then((response) => {
				resolve(response);
				commit('ADD_SUPERVISION_AGENTS', {"server": server, "supervision_agents": response.body.response});
			},
			(error) => {
				reject(error);
			});
		});
	}

	export const delete_supervisionAgent = ( {commit, state}, payload) => {
		// payload.server | payload.idx_agent
		return new Promise((resolve, reject) => {
			Vue.http.post('supervision/agent/del', {'ip' : payload.server.ip, 'agent_ip': payload.server.supervision_agents[payload.idx_agent].ip }).then((response) => {
				resolve(response);
				commit('DELETE_SUPERVISION_AGENT', {'server': payload.server, "idx_agent": payload.idx_agent});
			});
		});
	}

	export const get_supervision_conf = ( {commit, state}, server) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('supervision/conf', {'ip': server.ip}).then((response) => {
				resolve(response);
				commit('ADD_SUPERVISION_CONF', {"server" : server, "supervision_config": response.body.response});
			},
			(error) => {
				reject(error);
			});
		});
	}

	export const edit_supervision_conf = ( {commit, state}, payload) =>{
		return new Promise((resolve, reject) => {
			Vue.http.post('supervision/conf/edit', {"ip": payload.server.ip, 'DBName': payload.new_conf.DBName, 'DBUser': payload.new_conf.DBUser, 'DBPassword': payload.new_conf.DBPassword}).then((response) => {
				resolve(response);
				commit('EDIT_SUPERVISION_CONF', {'server': payload.server, 'new_conf': payload.new_conf});
			},
			(error) => {
				reject(error);
			});
		});
	}

	export const get_ticketing_conf = ( {commit, state}, server) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('bugtracker/conf', {'ip': server.ip}).then((response) => {
				resolve(response);
				commit('ADD_TICKETING_CONF', {"server" : server, "ticketing_config": response.body.response});
			},
			(error) => {
				reject(error);
			});
		});
	}

	export const edit_ticketing_conf = ( {commit, state}, payload) =>{
		return new Promise((resolve, reject) => {
			Vue.http.post('bugtracker/conf/edit', {"ip": payload.server.ip, 'conf': payload.new_conf}).then((response) => {
				resolve(response);
				commit('EDIT_TICKETING_CONF', {'server': payload.server, 'new_conf': payload.new_conf});
			},
			(error) => {
				reject(error);
			});
		});
	}

	export const to_update_device = ({commit, state}, payload) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('config/update/execute', {'ip': payload.server.ip}).then((response) => {
				resolve(response);
			},
			(error) => {
				reject(error);
			});
		});
	}

	export const get_LDAP_users = ( {commit, state}, server) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('ldap/users', {"ip": server.ip } ).then((response) => {
				resolve(response);
				response.body.response.forEach( function(element, index) {
					commit('ADD_LDAP_USER', { "server": server, "user": response.body.response[index] });
				});
			},
			(error) => {
				reject(response);
			});
		});
	}

	export const add_LDAP_user = ({commit, state}, payload) => {
		return new Promise((resolve, reject) => {
			console.log({"ip": payload.server.ip, "username": payload.username, "passwd": payload.password});
			Vue.http.post('ldap/user/add', {"ip": payload.server.ip, "username": payload.username, "passwd": payload.passwd}).then((response) => {
				resolve(response);
				let tmp = "uid=" + payload.username + ", ou=users,dc=eloryal,dc=fr";
				console.log(tmp);
				commit('ADD_LDAP_USER', {"server": payload.server, "user": {"username": payload.username, "user_dn": tmp}});
			},
			(error) => {
				reject(response);
			});
		});
	}

	export const delete_LDAP_user = ( {commit, state }, payload) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('ldap/user/delete', {"ip": payload.server.ip, "user_dn": payload.user_dn } ).then((response) => {
				commit('DELETE_LDAP_USER', {"server": payload.server, "id_user": payload.id_user});
				resolve(response);
			},
			(error) => {
				reject(response);
			});
		});
	}

	export const get_LDAP_OrganizationUnit = ( {commit, state }, server) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('ldap/groups', {"ip": server.ip } ).then((response) => {
				resolve(response);
				response.body.response.forEach( function(element, index) {
					commit('ADD_LDAP_ORGANIZATION_UNIT', { "server": server, "group": response.body.response[index] });
				});
			},
			(error) => {
				reject(response);
			});
		});
	}

	export const add_LDAP_group = ( {commit, state }, payload) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('ldap/group/add', { "ip": payload.server.ip, "group": payload.groupname, "member_dn": payload.member_dn} ).then((response) => {
				commit("ADD_LDAP_ORGANIZATION_UNIT", { "server": payload.server, "group": { "groupname": payload.groupname, "members":[payload.member_dn] } } );
				resolve(response);
			},
			(error) => {
				reject(response);
			});
		});
	}

	export const add_LDAP_usersInGroup = ( {commit, state }, payload) => {
		// payload.server | payload.idx_directory | payload.login
		return new Promise((resolve, reject) => {
			Vue.http.post('ldap/group/user/add', {"ip": payload.server.ip,
				"group": payload.server.ldap_groups[payload.idx_directory].groupname, "user_dn": payload.user.user_dn} ).then((response) => {
					commit("ADD_LDAP_USER_IN_GROUP", { "group": payload.server.ldap_groups[payload.idx_directory], "user": payload.user});
					resolve(response);
				},
				(error) => {
					reject(response);
				});
			});
	}

	export const delete_LDAP_usersInGroup = ( {commit, state }, payload) => {
		// payload.server | payload.idx_directory | payload.idx_user | payload.login
		console.log(payload, {"ip": payload.server.ip,
			"group": payload.server.ldap_groups[payload.idx_directory].groupname, "user_dn": payload.user_dn});
		return new Promise((resolve, reject) => {
			Vue.http.post('ldap/group/user/delete', {"ip": payload.server.ip,
				"group": payload.server.ldap_groups[payload.idx_directory].groupname, "user_dn": payload.user_dn} ).then((response) => {
					resolve(response);
					commit("DELETE_SFTP_USER_IN_DIRECTORY", { "group": payload.server.ldap_groups[payload.idx_directory], "idx_user": payload.idx_user});			},
					(error) => {
						reject(response);
					});
			});
	}

	export const get_samba_configuration = ( {commit, state}, payload) => {
		console.log(payload, {'ip': payload.server.ip});
		return new Promise((resolve, reject) => {
			Vue.http.post('samba/conf', {'ip': payload.server.ip}).then((response) => {
				commit('SAMBA_CONFIGURATION', {'server': payload.server, 'samba_conf': response.body.response});
				resolve(response);
			},
			(error) => {
				reject(response);
			});
		});
	}


	export const add_samba_share = ( {commit, state}, payload) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('samba/share/add', {'ip': payload.server.ip, 'share': payload.share}).then((response) => {
				commit('SAMBA_ADD_SHARE', {'server': payload.server, 'share': payload.share});
				resolve(response);
			},
			(error) => {
				reject(response);
			});
		});
	}


	export const update_samba_share = ( {commit, state}, payload) => {
		console.log("PASSSSED");
		console.log({'ip': payload.server.ip, 'old_share_name': payload.old_share_name, 'new_share': payload.new_share});
		return new Promise((resolve, reject) => {
			Vue.http.post('samba/share/update', {'ip': payload.server.ip, 'old_share_name': payload.old_share_name, 'new_share': payload.new_share}).then((response) => {
				commit('SAMBA_UPDATE_SHARE', {'server': payload.server, 'old_share_name': payload.old_share_name, 'new_share': payload.new_share});
				resolve(response);
			},
			(error) => {
				reject(response);
			});
		});
	}

	export const delete_samba_share = ( {commit, state}, payload) => {
		return new Promise((resolve, reject) => {
			Vue.http.post('samba/share/delete', {'ip': payload.server.ip, 'share_name': payload.share_name}).then((response) => {
				commit('SAMBA_DELETE_SHARE', {'server': payload.server, 'idx_share': payload.idx_share, 'share_name': payload.share_name});
				resolve(response);
			},
			(error) => {
				reject(response);
			});
		});
	}

