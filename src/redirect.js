  import store from './store'
  
  export default {
    checkValidIdUrl(to) {
      if (!(store.getters.get_servers == null || to.params.id >= store.getters.get_servers.length || store.getters.get_servers[to.params.id] == null))
       return true;
     else {
      setTimeout(()=> {
        if (store.getters.get_servers != null && store.getters.get_servers.length >= to.params.id && store.getters.get_servers[to.params.id] != null) {
          return true
        } else {
          return false;
        }
      }, 1500);
    }
  },

  checkServiceAndId(to, service) {
    if (store.getters.get_servers != null && store.getters.get_servers != undefined && store.getters.get_servers.length > to.params.id && store.getters.get_servers[to.params.id] != undefined &&
     store.getters.get_servers[to.params.id].service === service)
      return true;
    else
      return false;
  }
}